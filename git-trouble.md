How to Git into Trouble
========================

Conceptual git for beginners

Paul Nijjar

2019-11-04













A Conversation 
--------------

Me: Hey Andrew! Add these links to <https://wrdashboard.ca>!

Andrew: What? Add them yourself.

Me: sudo Hey Andrew! Add these links to <https://wrdashboard.ca>!

Andrew: Paul is not in the sudoers list. This incident will be
reported.

Me: How do I get these links added to <https://wrdashboard.ca>??

Andrew: Do a pull request!

Me: ... 

Me: Okay, I guess. I'll learn how to make a pull request.

Me: HOW HARD COULD IT BE?



Version Control 
---------------

### Ideas

- Keep track of changes you made to files.
- Keep track of changes OTHER people make to your files.
- Branch your files into alternative universes to try new things or
  develop dangerous features.

Version control is usually associated with software source code, 
but it does not have to be. 

There are many version control systems, but git won.

### WHY???

- Go back and find where you introduced bugs
- Allow many people to work on the same set of files simultaneously
- Reduce worries about "killing your babies"
- Roll back stupid ideas when necessary
- Support many different versions of your files at once



The funniest joke
-----------------

Q: How do you make a bajillion dollars on the internet?

A: Take version control and... 


Goals
-----

- Learn enough git commands to make a pull request
- Learn enough about how git thinks to understand what it is trying to
  do 
  
  
Caveats
-------

- I do not know much about git (seriously)
- I am intentionally glossing over some details



Prerequisites
-------------

- You are okay with using the commandline
  + navigating folders
  + typing commands
  + using a text editor

- You have made a Github/Gitlab account

- You have associated an SSH key with that account, and have used
  `ssh-agent` to remember that key


The Rest of the Talk
--------------------

1. Creating your own repository and making commits
2. Branching and merging
3. Working with Gitlab/Github
4. Forking somebody else's repository
5. Making a pull request

(Will we get there? WE SHALL SEE)


Demo Time
----------

(Rats. I forgot to put together a demo. Let's wing it...)

### Getting started

`mkdir presentation` to make a folder.

Add some files if you want, but you do not need to.

`cd presentation`

~~~
git status

fatal: Not a git repository (or any of the parent directories): .git
~~~

Git does not know about any of these files yet!

![No git yet](pix/00-nogit.svg)



### git init

Now we will make a repository: `git init`

See what is available: `git status`

~~~
git status
On branch master

Initial commit

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .git-trouble.md.swp
        extra01
        git-trouble.md
        pix/
        terminology.md

nothing added to commit but untracked files present (use "git add" to track)

git log 
fatal: bad default revision 'HEAD'
~~~

At this point, git knows that there is a repository.

It has made a branch `master`.

No files have been checked in (but some are available).

There is an index but it is empty.


### git add 

We can add one file at a time, or a bunch of files, or an entire folder 
of files, or everything.

NOTE: You need to add files EVERY time you want to check them in.

NOTE: git does not make a commit for every file. 

When you add a file git records a snapshot of that file and adds it to the index, so it can be committed.

~~~
git add git-trouble.md pix/00-nogit.svg 

git status

On branch master

Initial commit

Changes to be committed:
  (use "git rm --cached <file>..." to unstage)

        new file:   git-trouble.md
        new file:   pix/00-nogit.svg

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .git-trouble.md.swp
        extra01
        pix/01-gitinit.svg
        pix/drawing.svg
        terminology.md

~~~


![Added files](pix/02-gitadd.svg)


### git commit

Now we will commit a file. This tells git to record this set of changes. 

~~~
git commit -m "Add initial presentation and no git svg"

git status

On branch master
Changes not staged for commit:
  (use "git add <file>..." to update what will be committed)
  (use "git checkout -- <file>..." to discard changes in working directory)

        modified:   git-trouble.md

Untracked files:
  (use "git add <file>..." to include in what will be committed)

        .git-trouble.md.swp
        extra01
        pix/01-gitinit.svg
        pix/drawing.svg
        terminology.md

no changes added to commit (use "git add" and/or "git commit -a")

~~~

Wow! `git-trouble.md` is still changed! Because we made a change
since the last add.

~~~
git log

commit 01879361f8014ca8d80cfb6c7e4a27f2346dc50b
Author: Paul Nijjar <pnijjar@alumni.uwaterloo.ca>
Date:   Fri Nov 1 18:24:34 2019 -0400

    Add initial version of presentation and first picture.

~~~

(Also useful: `git log -p`)

`01879361f8014ca8d80cfb6c7e4a27f2346dc50b` is the commit ID. It is a hash of the contents of this commit. It is intended to UNIQUELY identify this commit. If one thing changes in these files then the commit ID will be 
completely different!

![First commit](pix/02-gitcommit.svg)


### Second commit 

Oh look we added more files. 

~~~
git add git-trouble.md pix

git reset HEAD pix/drawing.svg 

git commit -m "Showed first commit"

git status

git log 
~~~


![Add more files](pix/03-gitcommit.svg)


~~~

git add git-trouble pix/03-gitcommit.svg
git commit 
git status
git log

~~~

### Branching!

~~~
git branch -a

git branch fixheadings

git branch -a

  fixheadings
* master

git checkout fixheadings

git branch -a
~~~

I created a new branch and switched to it. I can switch back too, and add other info.

### Merging

Once you have branched you can merge the branches back together.

~~~ 
git checkout master
git merge fixheadings
~~~

You may have to fix up merge conficts (where git feels that commits in
two different branches overlap with each other). 

Edit the file and fix it up, then `git add` the file and `git commit`


You can then delete the branch from the listing (which does not delete all commits)

~~~
git branch -d fixheadings
~~~



Working with Gitlab
-------------------

Start by logging into a gitlab account. Create a new empty project. 
Say the identity is <https://gitlab.com/pnijjar/test01>. 

Then take your own files and upload them.


~~~
git remote add origin git@gitlab.com:/pnijjar/test01.git
git remote -v

git push --set-upstream origin --all 

git push origin --all 
~~~

Now you can make changes and push them to gitlab.

~~~
git add git-trouble.md

git commit
git push origin --all 
~~~

Alternatively, to only push the master branch: `git push origin master`


You can pull changes from gitlab: 

~~~
git fetch --all
git pull origin master
~~~

You can copy the repository on another computer and work on both at the same time.


